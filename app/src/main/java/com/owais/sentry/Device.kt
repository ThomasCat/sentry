package com.owais.sentry
import android.accounts.AccountManager
import android.app.ActivityManager
import android.app.AlarmManager
import android.app.admin.DevicePolicyManager
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.bluetooth.BluetoothManager
import android.bluetooth.BluetoothProfile.GATT
import android.content.Context
import android.content.Context.*
import android.content.Intent
import android.content.IntentFilter
import android.content.SharedPreferences
import android.content.pm.ApplicationInfo
import android.content.pm.PackageManager
import android.content.res.Resources
import android.database.Cursor
import android.hardware.Camera
import android.hardware.usb.UsbDevice
import android.hardware.usb.UsbManager
import android.icu.text.DecimalFormat
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.location.LocationManager
import android.media.AudioManager
import android.net.wifi.WifiInfo
import android.net.wifi.WifiManager
import android.os.*
import android.provider.ContactsContract
import android.provider.Settings
import android.provider.Settings.Secure
import android.telephony.SubscriptionManager
import android.telephony.TelephonyManager
import android.text.format.Formatter
import android.util.Log
import android.util.Patterns
import android.widget.Toast
import androidx.core.app.ActivityCompat
import com.jaredrummler.android.device.DeviceName
import com.scottyab.rootbeer.RootBeer
import java.io.*
import java.net.InetAddress
import java.net.NetworkInterface
import java.net.SocketException
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

var localIP = ""
var lat = ""
var lon = ""

fun deviceFullName(): String {
    fun String.capitalizeWords(): String = split(" ").map { it.capitalize() }.joinToString(" ")
    val name = DeviceName.getDeviceName(Build.DEVICE,"Null").toLowerCase().capitalizeWords()
    return if (name=="Null") {
        Build.MANUFACTURER.toLowerCase().capitalizeWords()+" "+Build.MODEL.toLowerCase().capitalizeWords()
    } else {
        Build.MANUFACTURER.capitalizeWords()+" "+name.capitalizeWords()
    }
}

fun deviceName(): String {
    fun String.capitalizeWords(): String = split(" ").map { it.capitalize() }.joinToString(" ")
    return DeviceName.getDeviceName(Build.DEVICE,"Null").toLowerCase().capitalizeWords()
}

fun deviceCodeName(): String {
    fun String.capitalizeWords(): String = split(" ").map { it.capitalize() }.joinToString(" ")
    return android.os.Build.DEVICE.capitalizeWords()
}

fun deviceCPU(): String {
    return android.os.Build.BOARD.toUpperCase()
}

fun deviceOwner(activity: MainActivity, context: Context): String? {
    try{
        val c: Cursor? = context.contentResolver.query(ContactsContract.Profile.CONTENT_URI, null, null, null, null)
        if (c != null) {
            c.moveToFirst()
        }
        val name = c?.getString(c.getColumnIndex("display_name"))
        if (c != null) {
            c.close()
        }
        return name.toString()
    }catch(ex: Exception){
        fun String.capitalizeWords(): String = split(" ").map { it.capitalize() }.joinToString(" ")
        return Build.TYPE.capitalizeWords()
    }
}

fun androidID(context: Context): String {
    return Secure.getString(context.contentResolver, Secure.ANDROID_ID)
}

fun androidVersion(): String{
    val version = android.os.Build.VERSION.RELEASE.toString()
    var versionName=""
    if (version == "7.0"||version=="7.1"||version=="7.1.1"||version=="7.1.2"){
        versionName="Nougat"
    }else if (version == "8.0"||version=="8.1"||version=="8.1.0"){
        versionName="Oreo"
    }else if (version == "9.0"||version=="9"){
        versionName="Pie"
    }else if (version == "10"||version=="10.0"){
        versionName="Q"
    }else if (version == "11"||version=="11.0") {
        versionName = "R"
    }else if (version == "12"||version=="12.0"){
        versionName="S"
    }else if (version == "13"||version=="13.0"){
        versionName="T"
    }else if (version == "14"||version=="14.0"){
        versionName="U"
    }else if (version == "15"||version=="15.0"){
        versionName="V"
    }
    return versionName+" ("+version+")"
}

fun securityPatch(): String {
    return android.os.Build.VERSION.SECURITY_PATCH
}

fun buildNumber(): String {
    return android.os.Build.ID
}

fun workProfile(context: Context): String {
    val pm: PackageManager = context.packageManager
    var status=""
    status = if (!pm.hasSystemFeature(PackageManager.FEATURE_MANAGED_USERS)) {
        "Unsupported"
        // This device does not support work profiles!
    } else {
        "Supported"
    }
    return status
}

fun deviceResolution(): String {
    val height = Resources.getSystem().displayMetrics.heightPixels.toString()
    val width = Resources.getSystem().displayMetrics.widthPixels.toString()
    val dpi = Resources.getSystem().displayMetrics.density
    var dpiLevel=""
    when {
        dpi >= 4.0 -> {
            dpiLevel = "xxxhdpi"
        }
        dpi >= 3.0 -> {
            dpiLevel = "xxhdpi"
        }
        dpi >= 2.0 -> {
            dpiLevel = "xhdpi"
        }
        dpi >= 1.5 -> {
            dpiLevel = "hdpi"
        }
        dpi >= 1.0 -> {
            dpiLevel = "mdpi"
        }
        dpi >= 0.0 -> {
            dpiLevel = "ldpi"
        }
    }
    return height +"×"+width+" ("+dpiLevel+")"
}

fun deviceTemp(context: Context): String{
    var temp=""
    val reader1 = RandomAccessFile("/sys/devices/virtual/thermal/thermal_zone0/temp", "r")
    val reader2 = RandomAccessFile("/sys/devices/virtual/thermal/thermal_zone1/temp", "r")
    val reader3 = RandomAccessFile("/sys/devices/virtual/thermal/thermal_zone2/temp", "r")
    val reader4 = RandomAccessFile("/sys/devices/virtual/thermal/thermal_zone3/temp", "r")
    val reader5 = RandomAccessFile("/sys/devices/virtual/thermal/thermal_zone4/temp", "r")
    val reader6 = RandomAccessFile("/sys/devices/virtual/thermal/thermal_zone5/temp", "r")
    val reader7 = RandomAccessFile("/sys/devices/virtual/thermal/thermal_zone8/temp", "r")
    val temp1=reader1.readLine().toInt() / 1000
    val temp2=reader2.readLine().toInt() / 1000
    val temp3=reader3.readLine().toInt() / 1000
    val temp4=reader4.readLine().toInt() / 1000
    val temp5=reader5.readLine().toInt() / 1000
    val temp6=reader6.readLine().toInt() / 1000
    val temp7=reader7.readLine().toInt() / 1000
    if (temp1!=0){
        temp = temp1.toString()+"°C"
    }else if (temp2!=0){
        temp = temp2.toString()+"°C"
    }else if (temp3!=0){
        temp = temp3.toString()+"°C"
    }else if (temp4!=0){
        temp = temp4.toString()+"°C"
    }else if (temp5!=0){
        temp = temp5.toString()+"°C"
    }else if (temp6!=0){
        temp = temp6.toString()+"°C"
    }else if (temp7!=0){
        temp = temp7.toString()+"°C"
    }else{
        temp = "Error"
    }
    return temp
}

fun checkCamera(context: Context, param: String): String{
    var data=""
    if (param=="number"){
        val numCameras: Int = Camera.getNumberOfCameras()
        if (numCameras > 0) {
            data=numCameras.toString()+" cameras"
        }
    }else if (param=="availability"){
        var camera: Camera? = null
        try {
            camera = Camera.open()
        } catch (e: RuntimeException) {
            data=="busy"
        } catch (e: IOException) {
            data=="busy"
        } finally {
            if (camera != null) {camera?.release()}
        }
        data = if (data=="busy"){
            "In use"
        } else {
            "Not in use"
        }
    }
    return data
}

fun rootStatus(context: Context): String {
    val rootBeer = RootBeer(context)
    return if (rootBeer.isRooted) {
        "Rooted"
    } else {
        "Not Rooted"
    }
}

fun numberOfInstalledApps(context: Context): String {
    var numberOfNonSystemApps = 0
    val appList: List<ApplicationInfo> = context.packageManager.getInstalledApplications(0)
    for (info in appList) {
        if (info.flags and ApplicationInfo.FLAG_SYSTEM == 0) {
            numberOfNonSystemApps++
        }
    }
    return numberOfNonSystemApps.toString()
}

fun numberOfContacts(activity: MainActivity): String {
    var number = ""
    try {
        val cursor: Cursor =
            activity.managedQuery(ContactsContract.Contacts.CONTENT_URI, null, null, null, null)
        val count: Int = cursor.count
        number = count.toString()
    }catch (ex: Exception){
        number = "0"
    }
    return number
}

fun busyboxStatus(context: Context): String {
    val check = RootBeer(context)
    return if (check.checkForBusyBoxBinary()) {
        "Busybox installed"
    } else {
        "No Busybox"
    }
}


fun batteryPercentage(context: Context): String {
    val bm = context.getSystemService(Context.BATTERY_SERVICE) as BatteryManager?
    return bm!!.getIntProperty(BatteryManager.BATTERY_PROPERTY_CAPACITY).toString()+"%"
}

fun ramStats(context: Context): String {
    val activityManager = context.getSystemService(ACTIVITY_SERVICE) as ActivityManager
    val memoryInfo = ActivityManager.MemoryInfo()
    activityManager.getMemoryInfo(memoryInfo)
    val totalMemory = memoryInfo.totalMem / 1073741824.0
    val usedMemory = (memoryInfo.totalMem / 1073741824.0) - (memoryInfo.availMem / 1073741824.0)
    return DecimalFormat("#.#").format(usedMemory) +"GB / "+ DecimalFormat("#").format(totalMemory) +"GB"
}

fun usbStatus(context: Context): String {
    val intent = context.registerReceiver(null, IntentFilter(Intent.ACTION_BATTERY_CHANGED))
    val action: String? = intent?.action
    val plugged = intent!!.getIntExtra(BatteryManager.EXTRA_PLUGGED, -1)
    var state=""
    if (plugged == BatteryManager.BATTERY_PLUGGED_AC) {
        state="Charging"
    } else if ( plugged == BatteryManager.BATTERY_PLUGGED_USB ) {
        state="USB"
    } else if ( plugged == BatteryManager.BATTERY_PLUGGED_WIRELESS){
        state="Qi Charging"
    } else if (action.equals("android.hardware.usb.action.USB_DEVICE_ATTACHED", ignoreCase = true)){
        state="USB OTG"
    } else {
        state="Unplugged"
    }
    return state
}

fun otgVendorID(context: Context, intent: Intent): String{
    val intent = context.registerReceiver(null, IntentFilter(Intent.ACTION_BATTERY_CHANGED))
    val action: String? = intent?.action
    val plugged = intent!!.getIntExtra(BatteryManager.EXTRA_PLUGGED, -1)
    if (action.equals("android.hardware.usb.action.USB_DEVICE_ATTACHED", ignoreCase = true)){
        val device = intent.getParcelableExtra(UsbManager.EXTRA_DEVICE) as UsbDevice
        // device is always null
        if (device == null){
            return "Read Error"
        } else {
            return device.vendorId.toString()
        }
    } else {
        return "In device mode"
    }
}

fun sim1Name(context: Context): String {
    //get SIM provider
    var provider = ""
    try{
        if (ActivityCompat.checkSelfPermission(context, android.Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(context,"Please accept all permission requests from app settings.", Toast.LENGTH_LONG).show()
        }
        val subscriptionInfo = SubscriptionManager.from(context).activeSubscriptionInfoList
        provider = subscriptionInfo[0].carrierName.toString()
    }catch(ex: Exception){
        provider = "No SIM"
    }
    return provider
}

fun sim2Name(context: Context): String {
    //get SIM provider
    var provider = ""
    try{
        if (ActivityCompat.checkSelfPermission(context, android.Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(context,"Please accept all permission requests from app settings.", Toast.LENGTH_LONG).show()
        }
        val subscriptionInfo = SubscriptionManager.from(context).activeSubscriptionInfoList
        provider = subscriptionInfo[1].carrierName.toString()
    }catch(ex: Exception){
        provider = "No SIM"
    }
    return provider
}

fun sim1PhoneNumber(context: Context): String {
    //get SIM provider
    var phoneNo=""
    try {
        if (ActivityCompat.checkSelfPermission(context, android.Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(context,"Please accept all permission requests from app settings.", Toast.LENGTH_LONG).show()
        }
        val subscriptionInfo = SubscriptionManager.from(context).activeSubscriptionInfoList
        if (subscriptionInfo[0].number.toString()==""){
            phoneNo= "No phone number"
        }else{
            phoneNo=subscriptionInfo[0].number.toString()
        }
    }catch (ex: Exception){
        phoneNo = "No SIM"
    }
    return phoneNo
}

fun sim2PhoneNumber(context: Context): String {
    //get SIM provider
    var phoneNo=""
    try {
        if (ActivityCompat.checkSelfPermission(context, android.Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(context,"Please accept all permission requests from app settings.", Toast.LENGTH_LONG).show()
        }
        val subscriptionInfo = SubscriptionManager.from(context).activeSubscriptionInfoList
        if (subscriptionInfo[1].number.toString()==""){
            phoneNo= "No phone number"
        }else{
            phoneNo=subscriptionInfo[1].number.toString()
        }
    }catch (ex: Exception){
        phoneNo = "No SIM"
    }
    return phoneNo
}

fun sim1ICCID(context: Context): String {
    //get SIM provider
    var iccid = ""
    try {
        if (ActivityCompat.checkSelfPermission(context, android.Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(context, "Please accept all permission requests from app settings.", Toast.LENGTH_LONG).show()
        }
        val subscriptionInfo = SubscriptionManager.from(context).activeSubscriptionInfoList
        iccid = subscriptionInfo[0].iccId.toString()
    }catch (ex: Exception){
        iccid="No SIM"
    }
    return iccid
}

fun sim2ICCID(context: Context): String {
    //get SIM provider
    var iccid = ""
    try {
        if (ActivityCompat.checkSelfPermission(context, android.Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(context, "Please accept all permission requests from app settings.", Toast.LENGTH_LONG).show()
        }
        val subscriptionInfo = SubscriptionManager.from(context).activeSubscriptionInfoList
        iccid = subscriptionInfo[1].iccId.toString()
    }catch (ex: Exception){
        iccid="No SIM"
    }
    return iccid
}

fun sim1Country(context: Context): String {
    //get SIM provider
    var country=""
    try{
        if (ActivityCompat.checkSelfPermission(context, android.Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(context,"Please accept all permission requests from app settings.", Toast.LENGTH_LONG).show()
        }
        val subscriptionInfo = SubscriptionManager.from(context).activeSubscriptionInfoList
        val countryISO=subscriptionInfo[0].countryIso.toString()
        val countryName = Locale("", countryISO)
        country= countryName.displayCountry.toString()
    }catch (ex: Exception){
        country = "No SIM"
    }
    return country
}

fun sim2Country(context: Context): String {
    //get SIM provider
    var country=""
    try{
        if (ActivityCompat.checkSelfPermission(context, android.Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(context,"Please accept all permission requests from app settings.", Toast.LENGTH_LONG).show()
        }
        val subscriptionInfo = SubscriptionManager.from(context).activeSubscriptionInfoList
        val countryISO=subscriptionInfo[1].countryIso.toString()
        val countryName = Locale("", countryISO)
        country= countryName.displayCountry.toString()
    }catch (ex: Exception){
        country = "No SIM"
    }
    return country
}

fun getNumberOfWipes(context: Context): String{
    val prevDeviceInfoFile = "previous_info"
    val currentDeviceInfoFile = "current_info"
    val prevInfo: SharedPreferences = context.getSharedPreferences(prevDeviceInfoFile, Context.MODE_PRIVATE)
    val currentInfo: SharedPreferences = context.getSharedPreferences(currentDeviceInfoFile, Context.MODE_PRIVATE)
    var wipeStatus = ""
    if (prevInfo.getString("AndroidID", "") != currentInfo.getString("AndroidID", "")){
        wipeStatus = "Device wiped"
    } else {
        wipeStatus = "No wipes"
    }
    return wipeStatus
}

fun getEmail(context: Context, param: String): String {
    if (param=="email") {
        val emailPattern = Patterns.EMAIL_ADDRESS
        val accounts = AccountManager.get(context).accounts
        var acc = ""
        for (account in accounts) {
            if (emailPattern.matcher(account.name).matches()) {
                Log.d("MainActivity", String.format("%s - %s", account.name, account.type))
                acc = (account.name.replaceAfter("@", "")).replace("@", "")
            }
        }
        if (acc == ""){
            acc = "No account"
        }
        return acc
    } else if (param=="domain"){
        val emailPattern = Patterns.EMAIL_ADDRESS
        val accounts = AccountManager.get(context).accounts
        var acc = ""
        var domain = ""
        for (account in accounts) {
            if (emailPattern.matcher(account.name).matches()) {
                val TAG = "MainActivity"
                Log.d(TAG, String.format("%s - %s", account.name, account.type))
                acc= account.name.toString()
                if (acc.contains("gmail.com", ignoreCase = true)){
                    domain = "gmail.com"
                }else{
                    domain = "Not signed in"
                }
            }
        }
        if (domain == ""){
            domain = "No domain"
        }
        return domain
    }
    return ""
}

fun isEncrypted(context: Context): String {
    val devicePolicyManager: DevicePolicyManager = context.getSystemService(Context.DEVICE_POLICY_SERVICE) as DevicePolicyManager
    if (Build.VERSION.SDK_INT > Build.VERSION_CODES.HONEYCOMB) {
        val status: Int = devicePolicyManager.storageEncryptionStatus
        return if (DevicePolicyManager.ENCRYPTION_STATUS_ACTIVE === status) {
            "Encrypted"
        } else {
            "Unencrypted"
        }
    }
    return ""
}

fun getAvailableInternalSpace(): String {
    val path = Environment.getDataDirectory()
    val stat = StatFs(path.path)
    val blockSize = stat.blockSizeLong
    val availableBlocks = stat.availableBlocksLong
    val totalBlocks = stat.blockCountLong
    val freeSpace= (availableBlocks*blockSize)
    val freeSpaceGB = DecimalFormat("#.#").format((freeSpace.toFloat()) / 1000000000)
    val totalSpace= (totalBlocks*blockSize)
    val totalSpaceGB = DecimalFormat("#.#").format((totalSpace.toFloat()) / 1000000000)
    return freeSpaceGB.toString()+"GB / "+totalSpaceGB+"GB"
}

fun detectSDCard(context: Context): String {
    // do this only *after* you have checked external storage state:
    val storages: Array<File?> = context.getExternalFilesDirs(context.toString())
    return if (storages.size > 1 && storages[0] != null && storages[1] != null) {
        "Card Inserted"
    } else {
        "No SD Card"
    }
}

fun getSDCardSpace(context: Context): String {
    val storages: Array<File?> = context.getExternalFilesDirs(context.toString())
    return if (storages.size > 1 && storages[0] != null && storages[1] != null) {
        val path = Environment.getExternalStorageDirectory()
        val stat = StatFs(path.path)
        val blockSize = stat.blockSizeLong
        val totalBlocks = stat.blockCountLong.toFloat()/1048576000
        val availableBlocks = stat.availableBlocksLong.toFloat()/1048576000
        return DecimalFormat("#.#").format(availableBlocks).toString()+"GB / "+DecimalFormat("#.#").format(totalBlocks*blockSize).toString()+"GB"
    } else {
        "No space"
    }
}

fun getSDCardID(context: Context): String {
    val storages: Array<File?> = context.getExternalFilesDirs(context.toString())
    if (storages.size > 1 && storages[0] != null && storages[1] != null) {
        val path2 = "/sys/block/mmcblk0/device"
        //val serial: BufferedReader
        //val name: BufferedReader
        //val manfid: BufferedReader
        //val oemid: BufferedReader
        //val mfDate: BufferedReader
        val CID: BufferedReader
        //serial = BufferedReader(FileReader("$path2/serial"))
        //name = BufferedReader(FileReader("$path2/name"))
        //manfid = BufferedReader(FileReader("$path2/manfid"))
        //oemid = BufferedReader(FileReader("$path2/oemid"))
        //mfDate = BufferedReader(FileReader("$path2/date"))
        CID = BufferedReader(FileReader("$path2/cid"))
        //val sdSerial = serial.readLine()
        //val sdName = name.readLine()
        //val sdMfId = manfid.readLine()
        //val sdOemId = oemid.readLine()
        //val sdMfDate = mfDate.readLine()
        val sdCid = CID.readLine()
        return sdCid
    } else {
        return "No UUID"
    }
    return " "
}

fun lockDevice (context: Context): String {
    val pm: PowerManager? = context.getSystemService(POWER_SERVICE) as PowerManager?
    val currentDeviceInfoFile = "current_info"
    val currentInfo: SharedPreferences = context.getSharedPreferences(currentDeviceInfoFile, Context.MODE_PRIVATE)
    val editor:SharedPreferences.Editor =  currentInfo.edit()
    //var locks = currentInfo.getString("admin_locks", "0")
    var currentLocks = 0
    if (pm != null) {
        if (pm.isScreenOn()) {
             currentLocks = currentInfo.getInt("admin_locks", 0)
             currentLocks += 1
             editor.putInt("admin_locks", currentLocks)
             editor.commit()
            val policy = context.getSystemService(DEVICE_POLICY_SERVICE) as DevicePolicyManager?
            policy!!.lockNow()
        }
    }
    return ""
}

fun wifiSSID(context: Context?): String {
    val mWifiManager = (context!!.applicationContext.getSystemService(WIFI_SERVICE) as WifiManager)
    val info = mWifiManager.connectionInfo
    var ssid = info.ssid
    if (info.toString().contains("<unknown")){
        ssid= "Turn on WiFi and GPS"
    }else{
        ssid = info.ssid.replace("\"","")
    }
    return ssid.toString()
}

fun wifiLocalIP(context: Context): String {
    val wm = context.getSystemService(WIFI_SERVICE) as WifiManager
    var ip: String = (Formatter.formatIpAddress(wm.connectionInfo.ipAddress)).toString()
    if (ip == "0.0.0.0") {
        ip = "Disconnected"
    }
    localIP=ip
    return ip
}

fun mobileDataIP(): String {
    try {
        val interfaces: List<NetworkInterface> = Collections.list(NetworkInterface.getNetworkInterfaces())
        for (intf in interfaces) {
            val addrs: List<InetAddress> = Collections.list(intf.inetAddresses)
            for (addr in addrs) {
                if (!addr.isLoopbackAddress) {
                    return (addr.hostAddress.replaceAfter("%", "").replace("%", ""))
                }
            }
        }
    } catch (ex: java.lang.Exception) {
    } // for now eat exceptions
    return ""
}

fun wifiPublicIP(): String {
    val publicIP = try {
        val process = Runtime.getRuntime().exec("curl ifconfig.me")
        val reader = BufferedReader(InputStreamReader(process.inputStream))
        var read: Int
        val buffer = CharArray(4096)
        val output = StringBuffer()
        while (reader.read(buffer).also { read = it } > 0) {
            output.append(buffer, 0, read)
        }
        reader.close()
        process.waitFor()
        output.toString()
    } catch (e: IOException) {
        throw java.lang.RuntimeException(e)
    } catch (e: InterruptedException) {
        throw java.lang.RuntimeException(e)
    }
    return if(publicIP==""){
        "No IP"
    }else{
        publicIP
    }
}

fun wifiMAC(context: Context): String{
    val wifiManager =context.getSystemService(WIFI_SERVICE) as WifiManager
    val wInfo: WifiInfo = wifiManager.connectionInfo
    return wInfo.macAddress
}

fun bluetoothName(context: Context): String {
    var bluetoothname = ""
    val mBluetoothAdapter: BluetoothAdapter = BluetoothAdapter.getDefaultAdapter()
    if (mBluetoothAdapter == null) {
        // Device does not support Bluetooth
        bluetoothname = "Unsupported"
    } else if (!mBluetoothAdapter.isEnabled) {
        // Bluetooth is not enabled :)
        bluetoothname = "Disabled"
    } else {
        bluetoothname = Secure.getString(context.contentResolver, "bluetooth_name")
        // Bluetooth is enabled
    }
    return bluetoothname
}


fun bluetoothConnectedDevices(context: Context): String {
    val manager: BluetoothManager = context.getSystemService(BLUETOOTH_SERVICE) as BluetoothManager
    val connected: List<BluetoothDevice> = manager.getConnectedDevices(GATT)
    return connected.size.toString()
}

fun cellularDataType(context: Context): String {
    val mTelephonyManager: TelephonyManager = context.getSystemService(TELEPHONY_SERVICE) as TelephonyManager
    val networkType: Int = mTelephonyManager.networkType
    return when (networkType) {
        TelephonyManager.NETWORK_TYPE_GPRS, TelephonyManager.NETWORK_TYPE_EDGE, TelephonyManager.NETWORK_TYPE_CDMA, TelephonyManager.NETWORK_TYPE_1xRTT, TelephonyManager.NETWORK_TYPE_IDEN -> "2G"
        TelephonyManager.NETWORK_TYPE_UMTS, TelephonyManager.NETWORK_TYPE_EVDO_0, TelephonyManager.NETWORK_TYPE_EVDO_A, TelephonyManager.NETWORK_TYPE_HSDPA, TelephonyManager.NETWORK_TYPE_HSUPA, TelephonyManager.NETWORK_TYPE_HSPA, TelephonyManager.NETWORK_TYPE_EVDO_B, TelephonyManager.NETWORK_TYPE_EHRPD, TelephonyManager.NETWORK_TYPE_HSPAP -> "3G"
        TelephonyManager.NETWORK_TYPE_LTE -> "4G LTE"
        TelephonyManager.NETWORK_TYPE_NR -> "5G"
        else -> "Cellular off"
    }
}

fun vpnStatus(context: Context): String {
    var iface = ""
    try {
        for (networkInterface in Collections.list(NetworkInterface.getNetworkInterfaces())) {
            if (networkInterface.isUp) iface = networkInterface.name
            if (iface.contains("tun") || iface.contains("ppp") || iface.contains("pptp")) {
                return "VPN On"
            }
        }
    } catch (e1: SocketException) {
        e1.printStackTrace()
    }
    return "VPN Off"
}

fun vpnInterface(context: Context): String {
    var iface = ""
    try {
        for (networkInterface in Collections.list(NetworkInterface.getNetworkInterfaces())) {
            if (networkInterface.isUp) iface = networkInterface.name
            if (iface.contains("tun")){
                return "Tunnel"
            }else if (iface.contains("ppp")){
                return "PPP"
            }else if (iface.contains("pptp")){
                return "PPTP"
            }
        }
    } catch (e1: SocketException) {
        e1.printStackTrace()
    }
    return "No interface"
}

fun hotspotStatus(context: Context): String {
    val wifimanager = context.getSystemService(WIFI_SERVICE) as WifiManager
    val apState = wifimanager.javaClass.getMethod("getWifiApState").invoke(wifimanager) as Int
    var status = ""
    if (apState == 13 || apState == 12) {
        status = "Hotspot On"
        // Ap Enabled
    }else if (apState == 11 || apState == 10) {
        status = "Hotspot off"
    }else if (apState == 14) {
        status = "Unsupported"
    }
    return status
}

fun hotspotIP(): String {
    var ip = ""
    try {
        val enumNetworkInterfaces = NetworkInterface.getNetworkInterfaces()
        while (enumNetworkInterfaces.hasMoreElements()) {
            val networkInterface = enumNetworkInterfaces.nextElement()
            val enumInetAddress = networkInterface.inetAddresses
            while (enumInetAddress.hasMoreElements()) {
                val inetAddress = enumInetAddress.nextElement()
                if (inetAddress.isSiteLocalAddress) {
                    ip = inetAddress.hostAddress
                    if (ip==localIP){
                        ip="Disabled"
                    }
                }
            }
        }
    } catch (e: SocketException) {
        // TODO Auto-generated catch block
        ip = "Disabled"
    }
    return ip
}

fun nextAlarm(context: Context): String {
    try {
        val alarmval = context.getSystemService(AlarmManager::class.java).nextAlarmClock.triggerTime
        return (SimpleDateFormat("h:m a").format(Date(alarmval))).toString().toUpperCase()
    }catch (ex: Exception){
        return "No alarm set"
    }
    return ""
}

fun currentPreciseTime(): String{
    return SimpleDateFormat("hh:mm:ss a", Locale.getDefault()).format(Date()).toUpperCase()
}

fun currentDate(): String{
    return SimpleDateFormat("EEEE, MMMM d, y", Locale.getDefault()).format(Date())
}

fun currentTimeZone(): String{
    val calendar = Calendar.getInstance(TimeZone.getTimeZone("GMT"), Locale.getDefault())
    val currentLocalTime = calendar.time
    val date: DateFormat = SimpleDateFormat("ZZZZZ", Locale.getDefault())
    val localTime: String = date.format(currentLocalTime)
    val tz = TimeZone.getDefault()
    return tz.getDisplayName(false, TimeZone.SHORT)+" ("+localTime+")"
}

fun deviceLatitude(context: Context): String {
    if (ActivityCompat.checkSelfPermission(context, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
        Toast.makeText(context,"Please accept all permission requests from app settings.", Toast.LENGTH_LONG).show()
    }
    val locationManager: LocationManager = context.getSystemService(Context.LOCATION_SERVICE) as LocationManager
    val providers: List<String> = locationManager.getProviders(true)
    var location: Location? = null
    for (i in providers.size-1 downTo 0) {
         location= locationManager.getLastKnownLocation(providers[i])
         if (location != null){
            break
         }
    }
    val gps = DoubleArray(2)
    if (location != null) {
        gps[0] = location.latitude
        Log.e("gpsLat",gps[0].toString())
        if (gps[1]==0.0){
            lon="Error"
        }
    }
    lat = gps[0].toString()
    return lat
}

fun deviceLongitude(context: Context):String {
    if (ActivityCompat.checkSelfPermission(context, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
        Toast.makeText(context,"Please accept all permission requests from app settings.", Toast.LENGTH_LONG).show()
    }
    val locationManager: LocationManager = context.getSystemService(Context.LOCATION_SERVICE) as LocationManager
    val providers: List<String> = locationManager.getProviders(true)
    var location: Location? = null
    for (i in providers.size-1 downTo 0) {
        location= locationManager.getLastKnownLocation(providers[i])
        if (location != null){
            break
        }
    }
    val gps = DoubleArray(2)
    if (location != null) {
        gps[1] = location.longitude
        Log.e("gpsLon",gps[1].toString())
        if (gps[1]==0.0){
            lon="Error"
        }
    }
    lon=gps[1].toString()
    return lon
}

fun deviceCity(context: Context): String{
    var cityName = ""
    try {
        val gcd = Geocoder(context, Locale.getDefault())
        val addresses: List<Address> = gcd.getFromLocation(lat.toDouble(), lon.toDouble(), 1)
        if (addresses.isNotEmpty()) {
            cityName = addresses[0].subLocality
        }else{
            cityName = "Unable to find city"
        }
    } catch (ex: Exception) {
        return "Location Error"
    }
    return cityName
}

fun ringerMode(context: Context): String {
    var status = ""
    val am: AudioManager = context.getSystemService(AUDIO_SERVICE) as AudioManager
    if (am.ringerMode == 2) {
        status = "Ringer Mode"
    }else if (am.ringerMode == 1) {
        status = "Vibrate Mode"
    }else if (am.ringerMode == 0) {
        status = "Silent Mode"
    }
    return status
}

fun notificationMode(context: Context): String {
    var status = ""
    if (Settings.Global.getInt(context.contentResolver, "zen_mode")==0) {
        status = "Notifications On"
    } else if (Settings.Global.getInt(context.contentResolver, "zen_mode")==1) {
        status = "Priority Only"
    } else if (Settings.Global.getInt(context.contentResolver, "zen_mode")==2) {
        status = "Total Silence"
    } else if (Settings.Global.getInt(context.contentResolver, "zen_mode")==3) {
        status = "Alarms Only"
    }
    return status
}
