package com.owais.sentry

import android.Manifest.permission
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.net.Uri
import android.os.Bundle
import android.os.Vibrator
import android.provider.Settings
import android.text.Html
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import kotlinx.android.synthetic.main.activity_welcome.*
import kotlinx.android.synthetic.main.content_main.*

class WelcomeActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        val prefsFile = "preferences"
        val prefs: SharedPreferences = this.getSharedPreferences(prefsFile, Context.MODE_PRIVATE)
        val prefsEditor:SharedPreferences.Editor =  prefs.edit()

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_welcome)

        if (prefs.getString("permissions_granted", "FALSE")=="TRUE" && prefs.getString("admin_mode", "FALSE")=="TRUE") {
            if (prefs.getString("mode", "") == "Standalone" || prefs.getString("mode", "") == "Empire") {
                val intent = Intent(applicationContext, MainActivity::class.java)
                startActivity(intent)
                finish()
            } else {
                val intent = Intent(applicationContext, ModeSelection::class.java)
                startActivity(intent)
                finish()
                // Toast.makeText(applicationContext,prefs.getString("permissions_granted", "FALSE"), Toast.LENGTH_LONG).show()
            }
        }
        SourceCodeLink.setOnClickListener (View.OnClickListener{
            val vibratorService = getSystemService(Context.VIBRATOR_SERVICE) as Vibrator
            vibratorService.vibrate(100)
            var openUrl = Intent(Intent.ACTION_VIEW, Uri.parse("https://gitlab.com/ThomasCat/sentry"))
            startActivity(openUrl)
        })
NextButton.setOnClickListener (View.OnClickListener{
    val vibratorService = getSystemService(Context.VIBRATOR_SERVICE) as Vibrator
    vibratorService.vibrate(100)
    ////////////////////////////
    // Permissions explained START
    val builder = android.app.AlertDialog.Builder(this)
    builder.setTitle(Html.fromHtml("<font color='#FFFFFF'>Permissions required</font>"))
    builder.setMessage(Html.fromHtml("<font color='#FFFFFF'><b>Background:</b> To log system at regular intervals</font><br/><br/><font color='#FFFFFF'><b>Camera:</b> To periodically check if camera is being used by other applications (no photos taken)</font><br/><br/><font color='#FFFFFF'><b>Contacts:</b> To monitor if contacts are being deleted. </font><br/><br/><font color='#FFFFFF'><font color='#FFFFFF'><b>Device Administrator:</b> To quickly lock device if lost</font><br/><br/><b>Phone:</b> To monitor IMEI, Carrier Name and SIM Card ID</font><br/><br/><font color='#FFFFFF'><b>Storage:</b> To monitor disk encryption and log to file (no access to personal data)</font><br/><br/><font color='#FFFFFF'><b>Location:</b> To monitor stolen or spoofed device</font>"))
    builder.setPositiveButton(Html.fromHtml("<font color='#FF5900'>Understood</font>")){ _, _ ->
        Dexter.withActivity(this)
            .withPermissions(
                permission.READ_EXTERNAL_STORAGE,
                permission.WRITE_EXTERNAL_STORAGE,
                permission.ACCESS_FINE_LOCATION,
                permission.ACCESS_COARSE_LOCATION,
                permission.READ_CONTACTS,
                permission.CAMERA,
                permission.READ_PHONE_STATE
            )
            .withListener(object : MultiplePermissionsListener {
                override fun onPermissionsChecked(report: MultiplePermissionsReport) {
                    // check if all permissions are granted
                    if (report.areAllPermissionsGranted()) {
                        prefsEditor.putString("permissions_granted", "TRUE")
                        prefsEditor.commit()
                        val vibratorService = getSystemService(Context.VIBRATOR_SERVICE) as Vibrator
                        vibratorService.vibrate(100)
                        if (prefs.getString("admin_mode", "FALSE")=="FALSE"){
                            prefsEditor.putString("admin_mode", "TRUE")
                            prefsEditor.commit()
                            Toast.makeText(applicationContext, "Please enable device administration", Toast.LENGTH_LONG).show()
                            startActivity(Intent().setComponent(ComponentName("com.android.settings", "com.android.settings.DeviceAdminSettings")))
                            finish()
                        }
                    }
                    // check for permanent denial of any permission
                    if (report.isAnyPermissionPermanentlyDenied) {
                        prefsEditor.putString("permissions_granted", "FALSE")
                        prefsEditor.commit()
                        val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
                        intent.addCategory(Intent.CATEGORY_DEFAULT)
                        intent.data = Uri.parse("package:$packageName")
                        startActivity(intent)
                        val vibratorService = getSystemService(Context.VIBRATOR_SERVICE) as Vibrator
                        vibratorService.vibrate(200)
                        Toast.makeText(applicationContext, "Please enable device administration", Toast.LENGTH_LONG).show()
                        startActivity(Intent().setComponent(ComponentName("com.android.settings", "com.android.settings.DeviceAdminSettings")))
                        finish()
                        Toast.makeText(applicationContext,"Please accept all permission requests!", Toast.LENGTH_LONG).show()
                    }
                }
                override fun onPermissionRationaleShouldBeShown(permissions: List<PermissionRequest>, token: PermissionToken) {
                    token.continuePermissionRequest()
                }
            })
            .onSameThread()
            .check()
    }
    builder.setNegativeButton(Html.fromHtml("<font color='#FF5900'>I don't trust you</font>")){ _, _ ->
        builder.setMessage(Html.fromHtml("<font color='#FFFFFF'>This application uses the mentioned permissions to check if your device is behaving strange (for example, if an application is constantly using your camera in the background without your knowledge). This application is completely open source, and its data collection mechanism is completely transparent.<br/><br/>Your personal data (such as photos, files and contacts) is never read. To see everything collected, open the in-app log by tapping the <i>Last Checked</i> card.</font>"))
        builder.setPositiveButton(Html.fromHtml("<font color='#FF5900'>Okay</font>")){ _, _ ->

        }
        builder.setNegativeButton(Html.fromHtml("<font color='#FF5900'>Still don't like it</font>")){ _, _ ->
            val vibratorService = getSystemService(Context.VIBRATOR_SERVICE) as Vibrator
            vibratorService.vibrate(50)
            finish()
            Toast.makeText(applicationContext, "Please uninstall the app.\n\n¯\\_(ツ)_/¯", Toast.LENGTH_LONG).show()
        }
        val permissionsExplainer: android.app.AlertDialog? = builder.create()
        permissionsExplainer?.setCancelable(false)
        permissionsExplainer?.show()
        if (permissionsExplainer != null) {
            permissionsExplainer.window?.setBackgroundDrawableResource(R.color.colorPrimary)
        }
    }
    val permissionsExplainer: android.app.AlertDialog? = builder.create()
    permissionsExplainer?.setCancelable(false)
    permissionsExplainer?.show()
    if (permissionsExplainer != null) {
        permissionsExplainer.window?.setBackgroundDrawableResource(R.color.colorPrimary)
    }
    // Permissions explained END
    })
}
}
