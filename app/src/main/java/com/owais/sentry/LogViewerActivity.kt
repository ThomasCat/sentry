package com.owais.sentry

import android.content.Context
import android.os.Bundle
import android.os.Vibrator
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.internal.ContextUtils.getActivity
import kotlinx.android.synthetic.main.activity_log_viewer.*
import java.io.File
import java.io.InputStream

class LogViewerActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_log_viewer)
        val fileName = "/storage/emulated/0/Android/data/sentry/log.json"
        val inputStream: InputStream = File(fileName).inputStream()
        val lineList = mutableListOf<String>()

        inputStream.bufferedReader().forEachLine { lineList.add(it) }
        lineList.forEach{Output.text=it.replace(",", ",\n")}
    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        val vibratorService = getSystemService(Context.VIBRATOR_SERVICE) as Vibrator
        vibratorService.vibrate(25)
        return true
    }
}
