package com.owais.sentry

import android.content.Context
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.util.Log
import android.widget.Toast
import androidx.core.app.ActivityCompat
import org.json.JSONException
import org.json.JSONObject
import java.io.BufferedWriter
import java.io.File
import java.io.FileWriter
import java.io.Writer


/////////////////////////////
/////SHAREDPREFS LOGGER START

fun logDataToSharedPref(context: Context){
    val prevDeviceInfoFile = "previous_info"
    val currentDeviceInfoFile = "current_info"
    val prevDeviceInfo: SharedPreferences = context.getSharedPreferences(prevDeviceInfoFile, Context.MODE_PRIVATE)
    val currentDeviceInfo: SharedPreferences = context.getSharedPreferences(currentDeviceInfoFile, Context.MODE_PRIVATE)
    val prevDeviceInfoEditor: SharedPreferences.Editor =  prevDeviceInfo.edit()
    val currentDeviceInfoEditor: SharedPreferences.Editor =  currentDeviceInfo.edit()


    //
    //      PREVIOUS        CURRENT <----
    //          ^               |       |
    //          |               |       |      ----> JSON
    //          |_______________|       |      |
    //                                 NEW ____|
    //___________________________________________________
    // 1. copy everything from current file to previous file
    // 2. copy new data to current file
    // 3. copy current file to JSON


    // (1)
    prevDeviceInfoEditor.putString("root_state", currentDeviceInfo.getString("root_state", "NONE"))
    prevDeviceInfoEditor.putString("busybox_state", currentDeviceInfo.getString("busybox_state", "NONE"))
    prevDeviceInfoEditor.putString("sim1_name", currentDeviceInfo.getString("sim1_name", "NONE"))
    prevDeviceInfoEditor.putString("sim2_name", currentDeviceInfo.getString("sim2_name", "NONE"))
    prevDeviceInfoEditor.putString("sim1_phone", currentDeviceInfo.getString("sim1_phone", "NONE"))
    prevDeviceInfoEditor.putString("sim2_phone", currentDeviceInfo.getString("sim2_phone", "NONE"))
    prevDeviceInfoEditor.putString("sim1_country", currentDeviceInfo.getString("sim1_country", "NONE"))
    prevDeviceInfoEditor.putString("sim2_country", currentDeviceInfo.getString("sim2_country", "NONE"))
    prevDeviceInfoEditor.putString("sim1_iccid", currentDeviceInfo.getString("sim1_iccid", "NONE"))
    prevDeviceInfoEditor.putString("sim2_iccid", currentDeviceInfo.getString("sim2_iccid", "NONE"))
    prevDeviceInfoEditor.putString("device_name", currentDeviceInfo.getString("device_name", "NONE"))
    prevDeviceInfoEditor.putString("device_model", currentDeviceInfo.getString("device_model", "NONE"))
    prevDeviceInfoEditor.putString("device_res", currentDeviceInfo.getString("device_res", "NONE"))
    prevDeviceInfoEditor.putString("device_temp", currentDeviceInfo.getString("device_temp", "NONE"))
    prevDeviceInfoEditor.putString("device_cameras", currentDeviceInfo.getString("device_cameras", "NONE"))
    prevDeviceInfoEditor.putString("device_camera_status", currentDeviceInfo.getString("device_camera_status", "NONE"))
    prevDeviceInfoEditor.putString("battery", currentDeviceInfo.getString("battery", "NONE"))
    prevDeviceInfoEditor.putString("ram", currentDeviceInfo.getString("ram", "NONE"))
    prevDeviceInfoEditor.putString("cpu", currentDeviceInfo.getString("cpu", "NONE"))
    prevDeviceInfoEditor.putString("usb_status", currentDeviceInfo.getString("usb_status", "NONE"))
    prevDeviceInfoEditor.putString("otg_vendor_id", currentDeviceInfo.getString("otg_vendor_id", "NONE"))
    prevDeviceInfoEditor.putString("owner", currentDeviceInfo.getString("owner", "NONE"))
    prevDeviceInfoEditor.putString("android_id", currentDeviceInfo.getString("android_id", "NONE"))
    prevDeviceInfoEditor.putString("android_version", currentDeviceInfo.getString("android_version", "NONE"))
    prevDeviceInfoEditor.putString("patch_level", currentDeviceInfo.getString("patch_level", "NONE"))
    prevDeviceInfoEditor.putString("build", currentDeviceInfo.getString("build", "NONE"))
    prevDeviceInfoEditor.putString("work_profile", currentDeviceInfo.getString("work_profile", "NONE"))
    prevDeviceInfoEditor.putInt("locks", currentDeviceInfo.getInt("locks", 0))
    prevDeviceInfoEditor.putString("wipes", currentDeviceInfo.getString("wipes", "NONE"))
    prevDeviceInfoEditor.putString("no_of_apps", currentDeviceInfo.getString("no_of_apps", "NONE"))
    prevDeviceInfoEditor.putString("no_of_contacts", currentDeviceInfo.getString("no_of_contacts", "NONE"))
    prevDeviceInfoEditor.putString("gmail_id", currentDeviceInfo.getString("gmail_id", "NONE"))
    prevDeviceInfoEditor.putString("domain", currentDeviceInfo.getString("domain", "NONE"))
    prevDeviceInfoEditor.putString("encrypted_status", currentDeviceInfo.getString("encrypted_status", "NONE"))
    prevDeviceInfoEditor.putString("internal_storage_space", currentDeviceInfo.getString("internal_storage_space", "NONE"))
    prevDeviceInfoEditor.putString("external_storage_space", currentDeviceInfo.getString("external_storage_space", "NONE"))
    prevDeviceInfoEditor.putString("encryption", currentDeviceInfo.getString("encryption", "NONE"))
    prevDeviceInfoEditor.putString("external_storage_inserted", currentDeviceInfo.getString("external_storage_inserted", "NONE"))
    prevDeviceInfoEditor.putString("external_storage_uuid", currentDeviceInfo.getString("external_storage_uuid", "NONE"))
    prevDeviceInfoEditor.putString("wifi_ssid", currentDeviceInfo.getString("wifi_ssid", "NONE"))
    prevDeviceInfoEditor.putString("wifi_local_ip", currentDeviceInfo.getString("wifi_local_ip", "NONE"))
    prevDeviceInfoEditor.putString("wifi_public_ip", currentDeviceInfo.getString("wifi_public_ip", "NONE"))
    prevDeviceInfoEditor.putString("cellular_ip", currentDeviceInfo.getString("cellular_ip", "NONE"))
    prevDeviceInfoEditor.putString("bluetooth_status", currentDeviceInfo.getString("bluetooth_status", "NONE"))
    prevDeviceInfoEditor.putString("bluetooth_connected_devices", currentDeviceInfo.getString("bluetooth_connected_devices", "NONE"))
    prevDeviceInfoEditor.putString("cellular_type", currentDeviceInfo.getString("cellular_type", "NONE"))
    prevDeviceInfoEditor.putString("vpn_status", currentDeviceInfo.getString("vpn_status", "NONE"))
    prevDeviceInfoEditor.putString("vpn_interface", currentDeviceInfo.getString("vpn_interface", "NONE"))
    prevDeviceInfoEditor.putString("hotspot_status", currentDeviceInfo.getString("hotspot_status", "NONE"))
    prevDeviceInfoEditor.putString("hotspot_ip", currentDeviceInfo.getString("hotspot_ip", "NONE"))
    prevDeviceInfoEditor.putString("current_time", currentDeviceInfo.getString("current_time", "Not checked yet"))
    prevDeviceInfoEditor.putString("current_date", currentDeviceInfo.getString("current_date", "Check later"))
    prevDeviceInfoEditor.putString("current_timezone", currentDeviceInfo.getString("current_timezone", "NONE"))
    prevDeviceInfoEditor.putString("next_alarm", currentDeviceInfo.getString("next_alarm", "NONE"))
    prevDeviceInfoEditor.putString("latitude", currentDeviceInfo.getString("latitude", "NONE"))
    prevDeviceInfoEditor.putString("longitude", currentDeviceInfo.getString("longitude", "NONE"))
    prevDeviceInfoEditor.putString("city", currentDeviceInfo.getString("city", "NONE"))
    prevDeviceInfoEditor.putString("ringer_mode", currentDeviceInfo.getString("ringer_mode", "NONE"))
    prevDeviceInfoEditor.putString("notification_mode", currentDeviceInfo.getString("notification_mode", "NONE"))
    prevDeviceInfoEditor.commit()


    // (2)
    currentDeviceInfoEditor.putString("root_state", rootstate)
    currentDeviceInfoEditor.putString("busybox_state", busyboxstate)
    currentDeviceInfoEditor.putString("sim1_name", sim1name)
    currentDeviceInfoEditor.putString("sim2_name", sim2name)
    currentDeviceInfoEditor.putString("sim1_phone", sim1phoneno)
    currentDeviceInfoEditor.putString("sim2_phone", sim2phoneno)
    currentDeviceInfoEditor.putString("sim1_country", sim1country)
    currentDeviceInfoEditor.putString("sim2_country", sim2country)
    currentDeviceInfoEditor.putString("sim1_iccid", sim1iccid)
    currentDeviceInfoEditor.putString("sim2_iccid", sim2iccid)
    currentDeviceInfoEditor.putString("device_name", devicename)
    currentDeviceInfoEditor.putString("device_model", devicemodel)
    currentDeviceInfoEditor.putString("device_res", deviceres)
    currentDeviceInfoEditor.putString("device_temp", devicetemp)
    currentDeviceInfoEditor.putString("device_cameras", devicecameras)
    currentDeviceInfoEditor.putString("device_camera_status", devicecamerastatus)
    currentDeviceInfoEditor.putString("battery", battery)
    currentDeviceInfoEditor.putString("ram", ram)
    currentDeviceInfoEditor.putString("cpu", cpu)
    currentDeviceInfoEditor.putString("usb_status", usbstatus)
    currentDeviceInfoEditor.putString("otg_vendor_id", otgvendorid)
    currentDeviceInfoEditor.putString("owner", owner)
    currentDeviceInfoEditor.putString("android_id", androidid)
    currentDeviceInfoEditor.putString("android_version", androidversion)
    currentDeviceInfoEditor.putString("patch_level", patchlevel)
    currentDeviceInfoEditor.putString("build", build)
    currentDeviceInfoEditor.putString("work_profile", workprofile)
    currentDeviceInfoEditor.putInt("locks", locks)
    currentDeviceInfoEditor.putString("wipes", wipes)
    currentDeviceInfoEditor.putString("no_of_apps", noofapps)
    currentDeviceInfoEditor.putString("no_of_contacts", noofcontacts)
    currentDeviceInfoEditor.putString("gmail_id", gmailid)
    currentDeviceInfoEditor.putString("domain", domain)
    currentDeviceInfoEditor.putString("encrypted_status", encryptedstatus)
    currentDeviceInfoEditor.putString("internal_storage_space", internalstorage)
    currentDeviceInfoEditor.putString("external_storage_space", externalstoragesize)
    currentDeviceInfoEditor.putString("encryption", encryptedstatus)
    currentDeviceInfoEditor.putString("external_storage_inserted", externalstoragedetector)
    currentDeviceInfoEditor.putString("external_storage_uuid", externalstorageid)
    currentDeviceInfoEditor.putString("wifi_ssid", ssid)
    currentDeviceInfoEditor.putString("wifi_local_ip", wifilocalip)
    currentDeviceInfoEditor.putString("wifi_public_ip", wifipublicip)
    currentDeviceInfoEditor.putString("cellular_ip", cellularpublicip)
    currentDeviceInfoEditor.putString("bluetooth_status", btstatus)
    currentDeviceInfoEditor.putString("bluetooth_connected_devices", btconnecteddevices)
    currentDeviceInfoEditor.putString("cellular_type", cellulartype)
    currentDeviceInfoEditor.putString("vpn_status", vpnstatus)
    currentDeviceInfoEditor.putString("vpn_interface", vpninterface)
    currentDeviceInfoEditor.putString("hotspot_status", hotspotstatus)
    currentDeviceInfoEditor.putString("hotspot_ip", hotspotip)
    currentDeviceInfoEditor.putString("current_time", currentprecisetime)
    currentDeviceInfoEditor.putString("current_date", currentdate)
    currentDeviceInfoEditor.putString("current_timezone", currenttimezone)
    currentDeviceInfoEditor.putString("next_alarm", nextalarm)
    currentDeviceInfoEditor.putString("latitude", latitude)
    currentDeviceInfoEditor.putString("longitude", longitude)
    currentDeviceInfoEditor.putString("city", city)
    currentDeviceInfoEditor.putString("ringer_mode", ringer)
    currentDeviceInfoEditor.putString("notification_mode", notificationmode)
    currentDeviceInfoEditor.commit()


}

fun logDataToJSON(context: Context){
    val `object` = JSONObject()
    try {
        `object`.put("root_state", rootstate)
        `object`.put("busybox_state", busyboxstate)
        `object`.put("sim1_name", sim1name)
        `object`.put("sim2_name", sim2name)
        `object`.put("sim1_phone", sim1phoneno)
        `object`.put("sim2_phone", sim2phoneno)
        `object`.put("sim1_country", sim1country)
        `object`.put("sim2_country", sim2country)
        `object`.put("sim1_iccid", sim1iccid)
        `object`.put("sim2_iccid", sim2iccid)
        `object`.put("device_name", devicename)
        `object`.put("device_model", devicemodel)
        `object`.put("device_res", deviceres)
        `object`.put("device_temp", devicetemp)
        `object`.put("device_cameras", devicecameras)
        `object`.put("device_camera_status", devicecamerastatus)
        `object`.put("battery", battery)
        `object`.put("ram", ram)
        `object`.put("cpu", cpu)
        `object`.put("usb_status", usbstatus)
        `object`.put("otg_vendor_id", otgvendorid)
        `object`.put("owner", owner)
        `object`.put("android_id", androidid)
        `object`.put("android_version", androidversion)
        `object`.put("patch_level", patchlevel)
        `object`.put("build", build)
        `object`.put("work_profile", workprofile)
        `object`.put("locks", locks)
        `object`.put("wipes", wipes)
        `object`.put("no_of_apps", noofapps)
        `object`.put("no_of_contacts", noofcontacts)
        `object`.put("gmail_id", gmailid)
        `object`.put("domain", domain)
        `object`.put("encrypted_status", encryptedstatus)
        `object`.put("internal_storage_space", internalstorage)
        `object`.put("external_storage_space", externalstoragesize)
        `object`.put("encryption", encryptedstatus)
        `object`.put("external_storage_inserted", externalstoragedetector)
        `object`.put("external_storage_uuid", externalstorageid)
        `object`.put("wifi_ssid", ssid)
        `object`.put("wifi_local_ip", wifilocalip)
        `object`.put("wifi_public_ip", wifipublicip)
        `object`.put("cellular_ip", cellularpublicip)
        `object`.put("bluetooth_status", btstatus)
        `object`.put("bluetooth_connected_devices", btconnecteddevices)
        `object`.put("cellular_type", cellulartype)
        `object`.put("vpn_status", vpnstatus)
        `object`.put("vpn_interface", vpninterface)
        `object`.put("hotspot_status", hotspotstatus)
        `object`.put("hotspot_ip", hotspotip)
        `object`.put("next_alarm", nextalarm)
        `object`.put("latitude", latitude)
        `object`.put("longitude", longitude)
        `object`.put("city", city)
        `object`.put("ringer_mode", ringer)
        `object`.put("notification_mode", notificationmode)
        `object`.put("current_timezone", currenttimezone)
        `object`.put("current_time", currentprecisetime)
        `object`.put("current_date", currentdate)
        ///Write JSON to file
        /////////////////////

        var output: Writer? = null
        val file = File("/storage/emulated/0/Android/data/sentry/log.json")
        val dir = File("/storage/emulated/0/Android/data/sentry")
        if (ActivityCompat.checkSelfPermission(context, android.Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(context,"Please accept all permission requests from app settings.", Toast.LENGTH_LONG).show()
        }

        val isNewFolderCreated: Boolean = dir.mkdirs()
        val isNewFileCreated :Boolean = file.createNewFile()

        if(isNewFileCreated&&isNewFolderCreated){
            Toast.makeText(context, "File & folder created", Toast.LENGTH_SHORT).show()
            output = BufferedWriter(FileWriter(file))
            output.write(`object`.toString())
            output.close()
            Toast.makeText(context, "Logged to file", Toast.LENGTH_SHORT).show()
        } else{
            Toast.makeText(context, "Detected file", Toast.LENGTH_SHORT).show()
            output = BufferedWriter(FileWriter(file))
            output.write(`object`.toString())
            output.close()
            Toast.makeText(context, "Logged to file", Toast.LENGTH_SHORT).show()
        }

        ///File write end
        ////////////////////
    } catch (e: Exception) {
        e.printStackTrace()
        Log.e("Exception", "File write failed: " + e.toString())
        Toast.makeText(context, "Unable to log. Please check permissions in settings.", Toast.LENGTH_LONG).show()
    }
    //println(`object`)

}


////SHAREDPREFS LOGGER END
////////////////////////////