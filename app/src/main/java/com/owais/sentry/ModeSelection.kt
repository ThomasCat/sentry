package com.owais.sentry

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Vibrator
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_mode_selection.*
import kotlinx.android.synthetic.main.activity_welcome.*

class ModeSelection : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_mode_selection)
        val prefsFile = "preferences"
        val prefs: SharedPreferences = this.getSharedPreferences(prefsFile, Context.MODE_PRIVATE)
        val prefsEditor: SharedPreferences.Editor =  prefs.edit()
        EmpireButton.setOnClickListener (View.OnClickListener{
            // prefsEditor.putString("mode", "Empire")
            // prefsEditor.commit()
            val vibratorService = getSystemService(Context.VIBRATOR_SERVICE) as Vibrator
            vibratorService.vibrate(100)
            // val intent = Intent(applicationContext, MainActivity::class.java)
            Toast.makeText(applicationContext,"Coming soon!", Toast.LENGTH_LONG).show()
            //startActivity(intent)
            //finish()
        })
        StandaloneButton.setOnClickListener (View.OnClickListener{
            val vibratorService = getSystemService(Context.VIBRATOR_SERVICE) as Vibrator
            vibratorService.vibrate(100)
            val intent = Intent(applicationContext, MainActivity::class.java)
            startActivity(intent)
            prefsEditor.putString("mode", "Standalone")
            prefsEditor.commit()
            finish()
        })
    }
}
