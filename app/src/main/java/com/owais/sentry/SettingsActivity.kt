package com.owais.sentry

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.net.Uri
import android.os.Bundle
import android.os.Vibrator
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_settings.*
import kotlinx.android.synthetic.main.activity_welcome.*
import java.lang.Boolean.TRUE


class SettingsActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings)
        setSupportActionBar(findViewById(R.id.Toolbar))
        actionBar?.setDisplayHomeAsUpEnabled(TRUE)
        actionBar?.setDisplayShowHomeEnabled(TRUE)
        }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        val vibratorService = getSystemService(Context.VIBRATOR_SERVICE) as Vibrator
        vibratorService.vibrate(25)
        return true
    }
}
