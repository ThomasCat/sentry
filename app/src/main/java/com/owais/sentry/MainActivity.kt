package com.owais.sentry

import android.annotation.SuppressLint
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Build
import android.os.Bundle
import android.os.Vibrator
import android.text.TextUtils
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_main.*


var devicefullname = ""
var rootstate = ""
var busyboxstate = ""
var sim1name = ""
var sim2name = ""
var sim1phoneno = ""
var sim2phoneno = ""
var sim1country = ""
var sim2country = ""
var sim1iccid = ""
var sim2iccid = ""
var devicename = ""
var devicemodel = ""
var deviceres = ""
var devicetemp = ""
var devicecameras = ""
var devicecamerastatus = ""
var battery = ""
var ram = ""
var cpu = ""
var usbstatus = ""
var otgvendorid = ""
var owner = ""
var androidid = ""
var androidversion = ""
var patchlevel = ""
var build = ""
var workprofile = ""
var locks = 0
var wipes = ""
var noofapps = ""
var noofcontacts = ""
var gmailid = ""
var domain = ""
var encryptedstatus = ""
var internalstorage = ""
var externalstoragedetector = ""
var externalstoragesize = ""
var externalstorageid = ""
var ssid = ""
var wifilocalip = ""
var wifipublicip = ""
var cellularpublicip = ""
var wifimac = ""
var btstatus = ""
var btconnecteddevices = ""
var cellulartype = ""
var vpnstatus = ""
var vpninterface = ""
var hotspotstatus = ""
var hotspotip = ""
var currentprecisetime = ""
var currentdate = ""
var currenttimezone = ""
var nextalarm = ""
var latitude = ""
var longitude = ""
var city = ""
var ringer = ""
var notificationmode = ""


class MainActivity : AppCompatActivity() {
    @SuppressLint("SetTextI18n")
    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)
        val prefsFile = "preferences"
        val prevDeviceInfoFile = "previous_info"
        val currentDeviceInfoFile = "current_info"
        val prefs: SharedPreferences = this.getSharedPreferences(prefsFile, Context.MODE_PRIVATE)
        val prevDeviceInfo: SharedPreferences = this.getSharedPreferences(prevDeviceInfoFile, Context.MODE_PRIVATE)
        val currentDeviceInfo: SharedPreferences = this.getSharedPreferences(currentDeviceInfoFile, Context.MODE_PRIVATE)
        val prefsEditor:SharedPreferences.Editor =  prefs.edit()
        val prevDeviceInfoEditor:SharedPreferences.Editor =  prevDeviceInfo.edit()
        val currentDeviceInfoEditor:SharedPreferences.Editor =  currentDeviceInfo.edit()
        //SHAREDPREFS INITIALIZATION END///////////////////////////////

        ////WELCOME//////////////////////////////////////
        if (prefs.getString("mode", "Standalone")=="Standalone"){
            OnlineServiceProviderCardLabel.text = prefs.getString("mode", "Standalone")
            SyncStatus.text = "Notifying"
            ConnectionStatus.setImageResource(R.drawable.ic_disconnected)
            SyncInterval.text = prefs.getString("interval", "Always on")
        } else if (prefs.getString("mode", "")=="Empire"){
            OnlineServiceProviderCardLabel.text = prefs.getString("mode", "Standalone")
            SyncStatus.text = "Syncing"
            ConnectionStatus.setImageResource(R.drawable.ic_connected)
            SyncInterval.text = prefs.getString("interval", "Real-time")
        }

        ///WELCOME END///////////////////////////////////

        ///////////////////////////
        // Data grabbers START


        devicefullname = deviceFullName()
        rootstate = rootStatus(applicationContext)
        busyboxstate = busyboxStatus(applicationContext)
        sim1name= sim1Name(applicationContext)
        sim2name= sim2Name(applicationContext)
        sim1phoneno= sim1PhoneNumber(applicationContext)
        sim2phoneno= sim2PhoneNumber(applicationContext)
        sim1country= sim1Country(applicationContext)
        sim2country= sim2Country(applicationContext)
        sim1iccid= sim1ICCID(applicationContext)
        sim2iccid= sim2ICCID(applicationContext)
        devicename= deviceName()
        devicemodel= deviceCodeName()
        deviceres= deviceResolution()
        devicetemp= deviceTemp(applicationContext)
        devicecameras= checkCamera(applicationContext, "number")
        devicecamerastatus= checkCamera(applicationContext, "availability")
        battery = batteryPercentage(applicationContext)
        ram = ramStats(applicationContext)
        cpu = deviceCPU()
        usbstatus= usbStatus(applicationContext)
        otgvendorid = otgVendorID(applicationContext, Intent(this, SettingsActivity::class.java))
        owner = deviceOwner(this, applicationContext).toString()
        androidid = androidID(applicationContext)
        androidversion = androidVersion()
        patchlevel = securityPatch()
        build = buildNumber()
        workprofile = workProfile(applicationContext)
        locks = currentDeviceInfo.getInt("admin_locks", 0)
        wipes = getNumberOfWipes(applicationContext)
        noofapps = numberOfInstalledApps(applicationContext)
        noofcontacts = numberOfContacts(this)
        gmailid = getEmail(applicationContext, "email")
        domain = getEmail(applicationContext, "domain")
        encryptedstatus = isEncrypted(applicationContext)
        internalstorage = getAvailableInternalSpace()
        externalstoragedetector = detectSDCard(applicationContext)
        externalstoragesize = getSDCardSpace(applicationContext)
        externalstorageid = getSDCardID(applicationContext)
        ssid = wifiSSID(applicationContext)
        wifilocalip = wifiLocalIP(applicationContext)
        wifipublicip = wifiPublicIP()
        cellularpublicip = mobileDataIP()
        wifimac = wifiMAC(applicationContext)
        btstatus = bluetoothName(applicationContext)
        btconnecteddevices = bluetoothConnectedDevices(applicationContext)
        cellulartype = cellularDataType(applicationContext)
        vpnstatus = vpnStatus(applicationContext)
        vpninterface = vpnInterface(applicationContext)
        hotspotstatus = hotspotStatus(applicationContext)
        hotspotip = hotspotIP()
        currentprecisetime = currentPreciseTime()
        currentdate = currentDate()
        currenttimezone = currentTimeZone()
        nextalarm = nextAlarm(applicationContext)
        latitude = deviceLatitude(applicationContext)
        longitude = deviceLongitude(applicationContext)
        city = deviceCity(applicationContext)
        ringer = ringerMode(applicationContext)
        notificationmode = notificationMode(applicationContext)

        // Data grabbers END
        ///////////////////////////////////////////////////////////




        ///////////////////////////////////////////////////////////
        // UI elements Setter
        LastCheckedTimestamp.text=" "+prevDeviceInfo.getString("current_time", "Not checked yet")
        LastCheckedDateStamp.text=" "+prevDeviceInfo.getString("current_date", "Check later")
        title = devicefullname
        RootStatus.text = rootstate
        RootStatus.ellipsize = TextUtils.TruncateAt.MARQUEE
        RootStatus.isSelected=true
        RootStatus.isSingleLine=true
        Busybox.text = busyboxstate
        Busybox.ellipsize = TextUtils.TruncateAt.MARQUEE
        Busybox.isSelected=true
        Busybox.isSingleLine=true
        Sim1.ellipsize = TextUtils.TruncateAt.MARQUEE
        Sim1.isSelected=true
        Sim1.isSingleLine=true
        Sim1.text = " "+sim1name
        Sim2.ellipsize = TextUtils.TruncateAt.MARQUEE
        Sim2.isSelected=true
        Sim2.isSingleLine=true
        Sim2.text = " "+sim2name
        PhoneNumber1.text = " "+sim1phoneno
        PhoneNumber1.ellipsize = TextUtils.TruncateAt.MARQUEE
        PhoneNumber1.isSelected=true
        PhoneNumber1.isSingleLine=true
        PhoneNumber2.text = " "+sim2phoneno
        PhoneNumber2.ellipsize = TextUtils.TruncateAt.MARQUEE
        PhoneNumber2.isSelected=true
        PhoneNumber2.isSingleLine=true
        Country1.text = " "+sim1country
        Country1.ellipsize = TextUtils.TruncateAt.MARQUEE
        Country1.isSelected=true
        Country1.isSingleLine=true
        Country2.text = " "+sim2country
        Country2.ellipsize = TextUtils.TruncateAt.MARQUEE
        Country2.isSelected=true
        Country2.isSingleLine=true
        ICCID1.text = " ID: "+sim1iccid
        ICCID1.ellipsize = TextUtils.TruncateAt.MARQUEE
        ICCID1.isSelected=true
        ICCID1.isSingleLine=true
        ICCID2.text = " ID: "+sim2iccid
        DeviceName.text = " "+devicename
        DeviceName.ellipsize = TextUtils.TruncateAt.MARQUEE
        DeviceName.isSelected=true
        DeviceName.isSingleLine=true
        DeviceModel.text = " "+devicemodel
        DeviceModel.ellipsize = TextUtils.TruncateAt.MARQUEE
        DeviceModel.isSelected=true
        DeviceModel.isSingleLine=true
        DeviceResolution.text = " "+deviceres
        DeviceTemperature.text = " "+devicetemp
        Cameras.text = " "+devicecameras
        CameraStatus.text = " "+devicecamerastatus
        BatteryStatus.text = " "+battery
        RAMStatus.text = " "+ram
        CPUID.text = " "+cpu
        USBStatus.text = " "+usbstatus
        OTGVendor.text = " "+otgvendorid
        CurrentUser.ellipsize = TextUtils.TruncateAt.MARQUEE
        CurrentUser.isSelected=true
        CurrentUser.isSingleLine=true
        CurrentUser.text = " "+owner
        AndroidID.text= " Android ID: "+androidid
        AndroidVersion.text= " Version: "+androidversion
        PatchDate.text= " Patch: "+patchlevel
        BuildNumber.text= " Build: "+build
        WorkProfileStatus.text= " Work profile: "+workprofile
        AdminLocks.text = " "+locks+" locks"
        WipeStatus.text = " "+wipes
        InstalledApps.text = " "+noofapps+" apps"
        Contacts.text = " "+noofcontacts+" contacts"
        EmailID.ellipsize = TextUtils.TruncateAt.MARQUEE
        EmailID.isSelected=true
        EmailID.isSingleLine=true
        EmailID.text = " "+gmailid
        EmailDomain.text="@"+domain
        EncryptionStatus.text=" "+encryptedstatus
        InternalUsed.text=" "+internalstorage
        InternalUsed.ellipsize = TextUtils.TruncateAt.MARQUEE
        InternalUsed.isSelected=true
        InternalUsed.isSingleLine=true
        SDStatus.text=" "+externalstoragedetector
        SDStatus.ellipsize = TextUtils.TruncateAt.MARQUEE
        SDStatus.isSelected=true
        SDStatus.isSingleLine=true
        SDUsed.text=" "+ externalstoragesize
        SDUsed.ellipsize = TextUtils.TruncateAt.MARQUEE
        SDUsed.isSelected=true
        SDUsed.isSingleLine=true
        SDUUID.text=" "+ externalstorageid
        SDUUID.ellipsize = TextUtils.TruncateAt.MARQUEE
        SDUUID.isSelected=true
        SDUUID.isSingleLine=true
        SSID.text=" "+ ssid
        SSID.ellipsize = TextUtils.TruncateAt.MARQUEE
        SSID.isSelected=true
        SSID.isSingleLine=true
        WifiLocalIP.text=" Local IP: "+wifilocalip
        WifiLocalIP.ellipsize = TextUtils.TruncateAt.MARQUEE
        WifiLocalIP.isSelected=true
        WifiLocalIP.isSingleLine=true
        WifiPublicIP.text=" Public IP: "+wifipublicip
        WifiPublicIP.ellipsize = TextUtils.TruncateAt.MARQUEE
        WifiPublicIP.isSelected=true
        WifiPublicIP.isSingleLine=true
        CellularIP.text=" IPv6: "+cellularpublicip
        CellularIP.ellipsize = TextUtils.TruncateAt.MARQUEE
        CellularIP.isSelected=true
        CellularIP.isSingleLine=true
        WifiMAC.text=" MAC: "+wifimac
        WifiMAC.ellipsize = TextUtils.TruncateAt.MARQUEE
        WifiMAC.isSelected=true
        WifiMAC.isSingleLine=true
        BluetoothStatus.text = " "+btstatus
        BluetoothConnectedDevices.text = " "+btconnecteddevices+ " devices connected"
        CellularType.text = " "+cellulartype
        VPNStatus.text = " "+vpnstatus
        VPNInterface.text = " "+vpninterface
        HotspotStatus.text = " "+hotspotstatus
        HotspotIP.text = " "+hotspotip
        val thread: Thread = object : Thread() {
            override fun run() {
                try {
                    while (!this.isInterrupted) {
                        sleep(1000)
                        runOnUiThread {
                            Time.text = " "+ currentPreciseTime()
                            DateStamp.text = " "+ currentDate()
                            Latitude.text = " Lat: "+latitude
                            Longitude.text = " Lon: "+longitude
                            CityName.text = " "+city
                        }
                    }
                } catch (e: InterruptedException) {
                }
            }
        }
        thread.start()
        Timezone.text = " "+currenttimezone
        NextAlarm.text = " "+nextalarm
        CityName.ellipsize = TextUtils.TruncateAt.MARQUEE
        CityName.isSelected=true
        CityName.isSingleLine=true
        RingerVolume.text = " "+ringer
        RingerVolume.ellipsize = TextUtils.TruncateAt.MARQUEE
        RingerVolume.isSelected=true
        RingerVolume.isSingleLine=true
        DNDStatus.text = " "+notificationmode
        DNDStatus.ellipsize = TextUtils.TruncateAt.MARQUEE
        DNDStatus.isSelected=true
        DNDStatus.isSingleLine=true
        // UI elements END
        /////////////////////////////////////////////////////////


        logDataToSharedPref(applicationContext)
        logDataToJSON(applicationContext)



        SettingsButton.setOnClickListener { _ ->
            val intent= Intent(this, SettingsActivity::class.java)
            startActivity(intent)
            val vibratorService = getSystemService(Context.VIBRATOR_SERVICE) as Vibrator
            vibratorService.vibrate(25)
        }

        LoggingStatus.setOnClickListener { _ ->
            val intent= Intent(this, LogViewerActivity::class.java)
            startActivity(intent)
            val vibratorService = getSystemService(Context.VIBRATOR_SERVICE) as Vibrator
            vibratorService.vibrate(25)
        }

        RefreshIcon.setOnClickListener { _ ->
            logDataToSharedPref(applicationContext)
            logDataToJSON(applicationContext)
            val vibratorService = getSystemService(Context.VIBRATOR_SERVICE) as Vibrator
            vibratorService.vibrate(25)
        }

        AdminLocks.setOnClickListener { _ ->
            try {
                lockDevice(applicationContext)
            } catch (ex: SecurityException) {
                Toast.makeText(applicationContext, "Please enable device administration in system settings", Toast.LENGTH_LONG).show()
                startActivity(Intent().setComponent(ComponentName("com.android.settings", "com.android.settings.DeviceAdminSettings")))
            }
        }


    }
}
