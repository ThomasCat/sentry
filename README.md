# Sentry

An open source Android client for Security Information and Event Management.

Go to [releases](https://gitlab.com/ThomasCat/sentry/-/releases) page or [download latest version here](https://gitlab.com/ThomasCat/sentry/-/blob/master/app/release/app-release.apk)

